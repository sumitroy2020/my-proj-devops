﻿using NUnit.Framework;

namespace myapp.Tests
{
    [TestFixture]
    public class CalculatorTests
    {
        [SetUp]
        public void Setup()
        {

        }

        [Test]
        [TestCase(1,2 ,ExpectedResult =3)]
        [TestCase(1, 5, ExpectedResult = 6)]
        [TestCase(1, -3, ExpectedResult = -2)]
        [TestCase(1, 4, ExpectedResult = 0)]
        public int AddTwoNumber_Return_Int(int a,int b)
        {
            var cal = new Calculator();
            return  cal.Add(a,b);
            

        }
    }
}
